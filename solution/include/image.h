#ifndef _image_
#define _image_
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
} __attribute__((packed));

struct image {
    size_t width, height;
    struct pixel* pixels;
};

enum allocate_status{
  ALLOCATE_OK = 0,
  ALLOCATE_ERROR
};
struct maybe_image {
    bool valid;
    struct image image;
};

struct maybe_image create_image( size_t width, size_t height );

size_t get_size( const struct image picture );

void clear_picture_memory( struct image picture );

struct pixel get_pixel( const struct image* picture, size_t row, size_t col );

void set_pixel( struct image* picture, size_t row, size_t col, struct pixel pixel );

#endif
