#include "../include/file_handler.h"

const char* io_message[] = {
  [OPEN_ERR] = "Error occured when file has tried open.",
  [CLOSE_ERR] = "Error occured when file has tried close."
};

enum status_io file_open( FILE** const    in,
                          const char*     file_name,
                          const char*     mode )
{
  FILE* file = fopen( file_name, mode );
  if ( file ){ *in = file; return OPEN_OK; }
  else { return OPEN_ERR; }
}

enum status_io file_close( FILE* const     out )
{
  if ( fclose( out ) == EOF ){
    return CLOSE_ERR;
  } else { return CLOSE_OK; }
}
