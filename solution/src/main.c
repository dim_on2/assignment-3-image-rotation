#include "../include/file_handler.h"
#include "../include/image.h"
#include "../include/print_info.h"
#include "../include/rotate.h"
#include "../include/transform_bmp.h"
#include <stdio.h>

static enum status_io closing( FILE* const f1 ){
    enum status_io c_stat1 = file_close( f1 );
    if ( c_stat1 != CLOSE_OK ) println_info( io_message[c_stat1], stderr ); 
    return c_stat1;
}

static void dealloc( struct image i1, struct image i2 ){    
    clear_picture_memory( i1 );
    clear_picture_memory( i2 );
}

static void cleaning( FILE* const f1, FILE * const f2, struct image i1, struct image i2 ){
    dealloc( i1, i2 );
    closing( f1 );
    closing( f2 );
}

int main( int argc, char** args ) {

    if ( argc != 3 ){
        print_info( "Number of arguments isn't valid", stderr );
        return 1;
    }

    FILE* file_for_reading = NULL;
    FILE* file_for_writing = NULL;

    enum status_io o_stat1 = file_open( &file_for_reading, args[1], "r" );
    if ( o_stat1 != OPEN_OK ) { println_info( io_message[o_stat1], stderr ); return 1; }
    enum status_io o_stat2 = file_open( &file_for_writing, args[2], "w" );
    if ( o_stat2 != OPEN_OK ) { 
        println_info( io_message[o_stat2], stderr );
        closing( file_for_reading );
        return 1; 
    }

    struct image picture = {0};
    struct image rotate_picture = {0};

    enum read_status read_status = from_bmp( file_for_reading, &picture );
    if ( read_status != READ_OK ) { 
        println_info( output_message[read_status], stderr );
        cleaning( file_for_reading, file_for_writing, picture, rotate_picture );
        return 1;
    }

    enum allocate_status allocate_status = rotate( picture, &rotate_picture );
    if ( allocate_status != ALLOCATE_OK ){ 
        println_info( "Can't allocate image", stderr );
        cleaning( file_for_reading, file_for_writing, picture, rotate_picture );
        return 1; 
    }
   
    enum write_status write_status = to_bmp( file_for_writing, &rotate_picture );
    if ( write_status != WRITE_OK ) { 
        println_info( output_message[write_status], stderr );
        cleaning( file_for_reading, file_for_writing, picture, rotate_picture );
        return 1; 
    }

    enum status_io c_stat1 = closing( file_for_reading );
    enum status_io c_stat2 = closing( file_for_writing );
    dealloc( picture, rotate_picture );
    if ( c_stat1 != CLOSE_OK || c_stat2 != CLOSE_OK ) return 1;
    return 0;
}
